# EDF Tempo

Cette petite application permet de connaître la couleur du jour et du lendemain pour le tarif Tempo d'EDF.

EDF propose un contrat nommé "Tempo" qui offre des tarifs avantageux la plupart du temps. Mais en contre-partie, certaines journées sont facturées beaucoup plus chères. La tarification se décompose en trois tranches :
- les journées bleues appliquant une tarification très avantageuse ;
- les journées blanches appliquant une tarification avantageuse ;
- les journées rouges appliquant une tarification très très salée.
La couleur de la journée est annoncée la veille. Étant donné l’impact de la couleur de la journée sur la façon de consommer l’électricité, il devient nécessaire de se tenir informé de la couleur du jour et à venir. D’où cette application.

