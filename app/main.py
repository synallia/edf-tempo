__version__ = "0.1.0"

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ListProperty, ObjectProperty, StringProperty
from kivy.network.urlrequest import UrlRequest
from kivy.logger import Logger

from datetime import date


class CaseJour(BoxLayout):
    aujourdhui_ou_demain = StringProperty("Aujourd'hui ou demain")
    date_jour = StringProperty("1 jan.")
    couleur_jour = StringProperty("Couleur à venir")
    couleur_list = ListProperty([.75, .75, .75, 1.])

    def set_bleu(self):
        self.couleur_jour = "Jour bleu"
        self.couleur_list = [.0, .34, .8, 1.]

    def set_blanc(self):
        self.couleur_jour = "Jour blanc"
        self.couleur_list = [.96, .96, .96, 1.]

    def set_rouge(self):
        self.couleur_jour = "jour rouge"
        self.couleur_list = [.93, .3, .12, 1.]

    def set_non_defini(self):
        self.couleur_jour = "Couleur à venir"
        self.couleur_list = [.75, .75, .75, 1.]

    def set_couleur(self, code_couleur):
        if code_couleur == 'TEMPO_BLEU':
            self.set_bleu()
        elif code_couleur == 'TEMPO_BLANC':
            self.set_blanc()
        elif code_couleur == 'TEMPO_ROUGE':
            self.set_rouge()
        elif code_couleur == 'NON_DEFINI':
            self.set_non_defini()
        else:
            raise ValueError(f"Le code couleur {code_couleur} n'est pas reconnu.")


class AffichageTempo(BoxLayout):
    case_aujourdhui = ObjectProperty()
    case_demain = ObjectProperty()

    def set_couleurs(self):
        # Récupérer les couleurs
        url_api = f"https://particulier.edf.fr/services/rest/referentiel/searchTempoStore?dateRelevant={date.today().strftime('%Y-%m-%d')}"
        headers = {'Accept': '*/*'}

        # Comportement en cas de succès
        def got_json(req, resultat):
            # Appliquer les couleurs
            self.case_aujourdhui.set_couleur(resultat['couleurJourJ'])
            self.case_demain.set_couleur(resultat['couleurJourJ1'])

        # Comportement en cas d'échec
        def got_error(req, resultat):
            Logger.warning("Impossible de joindre le site d'EDF")

        req = UrlRequest(url_api, on_success=got_json, on_failure=got_error, on_error=got_error,
                         req_headers=headers, timeout=5)


class EdfTempoApp(App):
    def build(self):
        # Instanciation du widget principal
        affichage = AffichageTempo()
        # Récupération des couleurs du jour
        affichage.set_couleurs()
        return affichage


if __name__ == '__main__':
    EdfTempoApp().run()
